function(RENAME_ICON _name _dir _newname)
  file(COPY ${_name} DESTINATION ${CMAKE_BINARY_DIR}/${_dir})
  get_filename_component(_basename ${_name} NAME)
  file(RENAME ${CMAKE_BINARY_DIR}/${_dir}/${_basename} ${CMAKE_BINARY_DIR}/${_dir}/${_newname})
endfunction()

function(rename_icon_png_and_svg _name _newname)
  rename_icon("${_name}.png" "actions/16x16" "${_newname}.png")
  rename_icon("${_name}.svg" "actions/scalable" "${_newname}.svg")
endfunction()

function(RENAME_OVERLAY_ICON_PNG_AND_SVG _name _newname)
  rename_icon("${_name}.png" "emblems/8x8" "${_newname}.png")
  rename_icon("${_name}.svg" "emblems/scalable" "${_newname}.svg")
endfunction()

function(gif_to_png _gif _png)
  execute_process(COMMAND ${CONVERT_PROGRAM} ${CMAKE_CURRENT_SOURCE_DIR}/${_gif} ${CMAKE_BINARY_DIR}/${_png})
endfunction()

### fall back to this one if nothing else found in the code- namespace:
rename_icon_png_and_svg(other/unknown_obj code-unknown)

### Enum icons
rename_icon_png_and_svg(enum/enum_obj code-enum)
rename_icon_png_and_svg(enum/enum_protected_obj code-enum-protected)
rename_icon_png_and_svg(enum/enum_private_obj code-enum-private)
rename_icon_png_and_svg(enum/enum_default_obj code-enum-package-access)
rename_icon_png_and_svg(enum/enum_alt_obj code-enum-forward-declaration)
# enumerator could also use code-variable+emblem-code-enum
gif_to_png(gif/enumerator_obj.gif actions/16x16/code-variable-enumerator.png)
rename_overlay_icon_png_and_svg(enum/enum_tsk emblem-code-enum)

### Class icons
rename_icon_png_and_svg(class/innerclass_public_obj code-class)
rename_icon_png_and_svg(class/innerclass_protected_obj code-class-protected)
rename_icon_png_and_svg(class/innerclass_private_obj code-class-private)
rename_icon_png_and_svg(class/innerclass_default_obj code-class-package-access)
rename_icon_png_and_svg(class/classfo_obj code-class-forward-declaration)
rename_overlay_icon_png_and_svg(class/class_tsk emblem-code-class)

### Interface icons
rename_icon_png_and_svg(interface/innerinterface_public_obj code-interface)
rename_icon_png_and_svg(interface/innerinterface_protected_obj code-interface-protected)
rename_icon_png_and_svg(interface/innerinterface_private_obj code-interface-private)
rename_icon_png_and_svg(interface/innerinterface_default_obj code-interface-package-access)
rename_icon_png_and_svg(interface/intf_obj code-interface-forward-declaration)
rename_overlay_icon_png_and_svg(interface/interface_tsk emblem-code-interface)

### Variable icons
rename_icon_png_and_svg(variable/build_var_obj code-variable)
rename_icon_png_and_svg(variable/envvar_obj code-variable-environment)
rename_icon_png_and_svg(variable/field_public_obj code-variable-public)
rename_icon_png_and_svg(variable/field_protected_obj code-variable-protected)
rename_icon_png_and_svg(variable/field_private_obj code-variable-private)
rename_icon_png_and_svg(variable/field_default_obj code-variable-package-access)
rename_icon_png_and_svg(variable/localvariable_obj code-variable-local)
rename_icon_png_and_svg(variable/typevariable_obj code-variable-type)
gif_to_png(gif/var_declaration_obj.gif actions/16x16/code-variable-forward-declaration.png)


### Function icons
rename_icon_png_and_svg(function/public_co code-function)
rename_icon_png_and_svg(function/methpub_obj code-function-public)
rename_icon_png_and_svg(function/methpro_obj code-function-protected)
rename_icon_png_and_svg(function/methpri_obj code-function-private)
rename_icon_png_and_svg(function/methdef_obj code-function-package-access)
gif_to_png(gif/cdeclaration_obj.gif actions/16x16/code-function-forward-declaration.png)


### Emblems
rename_overlay_icon_png_and_svg(emblems/abstract_co emblem-code-abstract)
rename_overlay_icon_png_and_svg(emblems/constr_ovr emblem-code-constructor)
rename_overlay_icon_png_and_svg(emblems/default_co emblem-code-default)
rename_overlay_icon_png_and_svg(emblems/default_tsk emblem-code-package-access)
rename_overlay_icon_png_and_svg(emblems/export_co emblem-code-export)
rename_overlay_icon_png_and_svg(emblems/final_co emblem-code-final)
rename_overlay_icon_png_and_svg(emblems/friend_co emblem-code-friend)
rename_overlay_icon_png_and_svg(emblems/implm_co emblem-code-implement)
rename_overlay_icon_png_and_svg(emblems/internal_co emblem-code-internal)
rename_overlay_icon_png_and_svg(emblems/native_co emblem-code-native)
rename_overlay_icon_png_and_svg(emblems/over_co emblem-code-override)
rename_overlay_icon_png_and_svg(emblems/private_co emblem-code-private)
rename_overlay_icon_png_and_svg(emblems/protected_co emblem-code-protected)
rename_overlay_icon_png_and_svg(emblems/static_co emblem-code-static)
rename_overlay_icon_png_and_svg(emblems/synch_co emblem-code-synchronized)
rename_overlay_icon_png_and_svg(emblems/transient_co emblem-code-transient)
rename_overlay_icon_png_and_svg(emblems/volatile_co emblem-code-volatile)
gif_to_png(gif/c_ovr.gif emblems/8x8/emblem-code-const.png)


### OTHER icons
rename_icon_png_and_svg(other/annotation_alt_obj code-annotation-forward-declaration)
rename_icon_png_and_svg(other/annotation_obj code-annotation)
rename_icon_png_and_svg(other/annotation_protected_obj code-annotation-protected)
rename_icon_png_and_svg(other/annotation_private_obj code-annotation-private)
rename_icon_png_and_svg(other/annotation_default_obj code-annotation-package-access)
rename_overlay_icon_png_and_svg(other/annotation_tsk emblem-code-annotation)

rename_icon_png_and_svg(other/brkp_obj code-debug-breakpoint)
rename_icon_png_and_svg(other/brkpd_obj code-debug-breakpoint-interactive)
rename_icon_png_and_svg(other/brkpi_obj code-debug-breakpoint-disabled)

rename_icon_png_and_svg(other/import_obj code-import)
rename_icon_png_and_svg(other/correction_delete_import code-import-delete)

rename_icon_png_and_svg(other/implm_co code-implement)
rename_icon_png_and_svg(other/over_co code-override)

rename_icon_png_and_svg(other/html_tag_obj code-xml-tag)
rename_icon_png_and_svg(other/package_obj code-module)
rename_icon_png_and_svg(other/javadoc code-documentation)
rename_icon_png_and_svg(other/jdoc_tag_obj code-documentation-tag)
rename_icon_png_and_svg(other/templateprop_co code-text-template)

# OTHER CDT icons (still .gifs)
gif_to_png(gif/define_obj.gif actions/16x16/code-macro.png)
gif_to_png(gif/namespace_obj.gif actions/16x16/code-namespace.png)
gif_to_png(gif/typedef_obj.gif actions/16x16/code-typedef.png)
gif_to_png(gif/typedeffo_obj.gif actions/16x16/code-typedef-forward-declaration.png)
gif_to_png(gif/struct_obj.gif actions/16x16/code-struct.png)
gif_to_png(gif/structfo_obj.gif actions/16x16/code-struct-forward-declaration.png)
gif_to_png(gif/union_obj.gif actions/16x16/code-union.png)
gif_to_png(gif/unionfo_obj.gif actions/16x16/code-union-forward-declaration.png)
gif_to_png(gif/shad_co.gif actions/16x16/code-shadow.png) # TODO: doesn't seem to be a matching overlay icon
gif_to_png(gif/using_obj.gif actions/16x16/code-using.png)




###### UNUSED ICONS ###############
# These are not used yet but exist
# TODO: what should they be named?
rename_icon_png_and_svg(class/newclass_wiz code-class-new) # TODO: or rather code-new-class?
rename_icon_png_and_svg(enum/newenum_wiz code-enum-new) # TODO: or rather code-new-enum?
rename_icon_png_and_svg(interface/newint_wiz code-interface-new) # TODO: or rather code-new-interface?

rename_icon_png_and_svg(variable/genericvariable_obj code-todo-variable-generic)
rename_icon_png_and_svg(variable/compare_field code-todo-variable-compare)
rename_icon_png_and_svg(function/compare_method code-todo-function-compare)

gif_to_png(gif/filterDefines.gif actions/16x16/code-todo-filter-hide-macros.png)
gif_to_png(gif/filterInactive.gif actions/16x16/code-todo-filter-hide-inactive.png)
rename_icon_png_and_svg(other/filter_imported_elements code-todo-filter-hide-imports)
rename_icon_png_and_svg(other/static_co code-todo-filter-hide-static)
rename_icon_png_and_svg(other/fields_co code-todo-filter-hide-fields)
rename_icon_png_and_svg(other/localtypes_co code-todo-filter-hide-locals)

rename_icon_png_and_svg(other/variable_tab code-todo-expression)
rename_icon_png_and_svg(other/watchlist_view code-todo-watch)
rename_icon_png_and_svg(other/activity code-todo-activity)


# TODO: deprecated is a 16x16 emblem (and not used yet)
# Maybe the same can be achieved by adding a plain strikethrough effect to the icon?
